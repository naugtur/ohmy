// Didn't feel like pulling webpack to get the cookie parsing dependency, and maybe xhr.

//BEGIN es5, 3rdparty environment
const frontEnd = {
  propagate: (data) => {
    try {
      console.table(data)
    } catch (e) {
      // no modern console
    }

    Object.keys(data).forEach(function(k) {
      document.cookie = 'local_' + k + '=' + encodeURIComponent(data[k])
    })
  },
  // I know I should be using xhr instead of fetch here, or even a blind form submit, but motivation is finite and this is not going to production today
  // the URL to post to should be taken from document.currentScript with fallback to something for IE, but see above
  set: () => {
    fetch('https://ohmy.naugtur.workers.dev', {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'include',
      body: JSON.stringify({ name: name, quote: quote }),
    })
      .then(function(re) {
        return re.json()
      })
      .then(function(json) {
        console.log(json.ip)
      })
  },
}

//END es5, 3rdparty environment

const handlers = [
  {
    accepts: (req, cookie) => req.method.toLowerCase() === 'get' && cookie.name,
    execute: async (req, cookie) => {
      return serveScript(frontEnd.propagate, cookie)
    },
  },
  {
    accepts: (req, cookie) =>
      req.method.toLowerCase() === 'get' && !cookie.name,
    execute: async (req, cookie) => {
      return serveScript(frontEnd.set, cookie)
    },
  },
  {
    accepts: (req, cookie) => req.method.toLowerCase() === 'post',
    execute: async (req, cookie) => {
      const body = await req.text()
      data = JSON.parse(body)
      const response = new Response(
        JSON.stringify({
          ip: req.headers.get('cf-connecting-ip'),
        }),
        {
          headers: {
            'Access-Control-Allow-Origin': req.headers.get('origin'),
            'Access-Control-Allow-Credentials': 'true',
            'content-type': 'application/json',
          },
        },
      )
      getCookieHeaders(data).forEach(cookieLine =>
        response.headers.append('set-cookie', cookieLine),
      )
      return response
    },
  },
]

addEventListener('fetch', event => {
  event.respondWith(runHandler(event.request, handlers))
})

async function runHandler(request, handlers = []) {
  const cookie = cookieParser(request.headers.get('cookie'))

  const handler = handlers.find(handler => handler.accepts(request, cookie))
  return await handler.execute(request, cookie)
}

function serveScript(implementation, data) {
  return new Response(
    `;(${implementation.toString()})(${JSON.stringify(data)});`,
    {
      headers: { 'content-type': 'text/javascript' },
    },
  )
}

function cookieParser(c) {
  if (!c) {
    return {}
  }
  return c.split('; ').reduce((acc, pair) => {
    const kv = pair.split('=')
    acc[kv[0]] = kv[1]
    return acc
  }, Object.create(null)) // Take that, prototype polution
}

function getCookieHeaders(data) {
  const maxAge = 60 * 60 * 24 * 365 * 20 // If someone uses a computer longer than that without reinstalling, they're the kind of person who clears their cookies all the time
  //I should also use Expires= for IE8 etc. but since it's the response to fetch() anyway...
  return Object.entries(data).map(([k, v]) => {
    return `${encodeURIComponent(k)}=${encodeURIComponent(
      v,
    )}; HttpOnly; SameSite=none; Secure; Max-Age=${maxAge}`
  })
}
