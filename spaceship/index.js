const { eventForwarder } = require('./forwarder')
const settings = require('./settings')

const fastify = require('fastify')({
  logger: {
    level: settings.logLevel,
  },
})

fastify.get('/', async (request, reply) => {
  return { healthcheck: 1 }
})
// The Assignment clearly states there's no content-type in request
fastify.addHook('onRequest', async (request, reply) => {
  const type = request.headers['content-type']
  if (!type || type.indexOf('json') < 0) {
    request.headers['content-type'] = 'application/json'
  }
})
fastify.post('/', async (request, reply) => {
  const forward = eventForwarder(request.log)
  await Promise.all(request.body.events.map(forward))
  return 'ok'
})

const start = async () => {
  try {
    await fastify.listen(1337)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()
