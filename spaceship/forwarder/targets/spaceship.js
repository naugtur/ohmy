const settings = require('../../settings')
const axios = require('axios')

module.exports = {
  name: 'spaceship',
  async send(event, logger) {
    // I'd use a library but it's an assignment, right?
    const body = flatten(event)
    return await axios.post(`${settings.baseUrl}/spaceship/r`, body)
  },
  flatten,
}

function flatList(obj, prefix = '') {
  return Object.keys(obj).reduce((acc, key) => {
    if (Array.isArray(obj[key])) {
      acc.push({ key: prefix + key, value: obj[key].join(',') })
    } else if (typeof obj[key] === 'object') {
      acc = acc.concat(flatList(obj[key], prefix + key + '.'))
    } else {
      acc.push({ key: prefix + key, value: obj[key] })
    }
    return acc
  }, [])
}

function flatten(obj) {
  const assembled = flatList(obj).reduce((acc, entry) => {
    acc[entry.key] = entry.value
    return acc
  }, {})
  return assembled
}
