const settings = require('../../settings')
const axios = require('axios')

module.exports = {
  name: 'sky',
  async send(event, logger) {
    const body = Object.keys(event).reduce((acc, key) => {
      const value = event[key]
      if (key !== 't') {
        key = capitalize(key)
      }
      acc[key] = value
      return acc
    }, {})
    logger.debug(body)
    body['Lat-lon'] = await getMapcode(body['Lat-lon'])
    return await axios.post(
      `${settings.baseUrl}/skyanalytics/get`,
      body,
    )
  },
}

function capitalize(key) {
  return key.charAt(0).toUpperCase() + key.slice(1)
}

// Feels like something I'd cache locally, but spacecraft travel at an angle and the likelyhood of cache hits is loooow
async function getMapcode(latlon) {
  const result = await axios({
    url: `https://api.mapcode.com/mapcode/codes/${latlon.join(',')}`,
  })
  return result.data.international.mapcode
}
