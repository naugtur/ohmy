const settings = require('../../settings')
const axios = require('axios')

module.exports = {
  name: 'monitor',
  async send(event, logger) {
    const { timestamp, ...body } = event
    const time = Math.floor(timestamp / 1000)
    return await axios.put(
      `${settings.baseUrl}/m0nit0r.com/track_ship/${time}`,
      body,
    )
  },
}
