const forwarders = [
  require('./targets/spaceship'),
  require('./targets/monitor'),
  require('./targets/sky'),
]

module.exports = {
  eventForwarder: logger => async event => {
    await Promise.all(
      forwarders.map(fw =>
        fw.send(event, logger).catch(err => {
          logger.warn(`sending ${event.t} to ${fw.name} failed ${err.stack}`)
        }),
      ),
    ).then(results => logger.debug({results}))
  },
}
